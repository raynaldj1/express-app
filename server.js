var express = require('express')
var app = express()
var cors = require('cors')

const mockUsers = [{
    id: 1,
    username: "Levi",
    age: "9",
    school: "Teasley"
   
},
{   
    id: 2,
    username: "Lyanna",
    age: 2,
    school: "The Suziki School",

   
}
]

const Port = process.env.port || 3000

app.use(cors())

app.get('/api',(req,res)=>{
res.send({"msg":"hello"})
})

app.get('/api/users',(req,res)=>{
    res.send(mockUsers)
})

app.get('/api/products', (req,res)=>{
    res.send([
        {
            name: "Strawberry Lime",
            size: "2.5oz",
            price: "3.00",
            color: "Red with sparkles"
        },
        {
            name: "Hibiscus Lime",
            size: "2.5oz",
            price: "3.00",
            color: "Purple with black sparkles"
        },
        {
            name: "Mango Chile",
            size: "2.5oz",
            price: "3.00",
            color: "red with orange dots"
        },
        {
            name: "Watermelon Kiwi",
            size: "2.5oz",
            price: "3.00",
            color: "Light Green with red dots"
        }
    ])
})


app.get('/api/users/:id',(req,res,)=>{ 
    var params = req.params.id
    const found = mockUsers.find(mockUser => mockUser.id == params)

    if(!found){
        res.send('no user found')
    } else{
        res.send(found)
    }
        
     
    }) 
    

app.listen(Port, ()=>{
    console.log('Server up and running big dog')
})

